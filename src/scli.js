/*
 * File: src
 * Project: scli
 * File Created: Friday, 3rd May 2019 3:25:53 pm
 * Author: Mark Kennard (mark.kennard@gmail.com)
 * -----
 * Last Modified: Friday, 3rd May 2019 3:25:53 pm
 * Modified By: Mark Kennard (mark.kennard@gmail.com>)
 * -----
 * Copyright 2018 - 2019 Mark Kennard AKA Snowmeister
 */

import arg from 'arg';
import inquirer from 'inquirer';
import os from 'os';
import process from 'process';
import fs from 'fs-extra';
import chalk from 'chalk';

const config = {};
/**
 * parseCLIArguments -
 * This method parses terminal cli arguments into a nice object
 * that we can consume easily
 * @param {*} rawArgs - Object, passed in from terminal
 */
function parseCLIArguments(rawArgs) {
  const args = arg(
    {
      '--name': String
    },
    {
      argv: rawArgs.slice(2)
    }
  );
  return {
    name: args['--name']
  };
}

/**
 * promptForProjectOptions
 * This method takes the options passed in
 * from the cli, and asks questions about the new
 * project...
 *
 * @param {*} options - Object, passed in from terminal
 */
async function promptForProjectOptions(options) {
  const defaultProjectType = 'JavaScript';
  const defaultProjectName = 'new-scli-project';
  const questions = [];
  questions.push({
    type: 'list',
    name: 'template',
    message: 'Which type of project do we need?',
    choices: ['JavaScript', 'PHP'],
    default: defaultProjectType
  });
  if (!options.name) {
    questions.push({
      type: 'input',
      name: 'projectname',
      message: 'Are you sure you want to keep the default project name?',
      default: defaultProjectName
    });
  }
  const answers = await inquirer.prompt(questions);
  let outname;
  if (options.name) {
    outname = options.name;
  } else {
    if (answers.projectname !== '') {
      outname = answers.projectname;
    } else {
      outname = defaultProjectName;
    }
  }
  const returnObj = {
    ...options,
    template: options.template || answers.template,
    name: outname
  };
  return returnObj;
}

async function askUserForRepoPath(options) {
  const home = os.homedir();
  const questions = [];
  questions.push({
    type: 'input',
    name: 'repos_dir',
    message: 'It looks like this is the first time you have run scli. Please provide your repos directory to continue.',
    default: `${home}/repos`
  });
  const answers = await inquirer.prompt(questions);
  options.repo_dir = answers.repos_dir;
  return await createEnv(options, answers.repos_dir);
}

async function createEnv(options, path) {
  if (path === '' || path === undefined) {
    path = os.homedir() + '/repos';
  }

  const exists = await fs.pathExists(__dirname + '/config.json');
  if (!exists) {
    return await fs.writeFile(__dirname + '/config.json', `{"repo_dir": "${path}"}`, err => {
      /* doh! throws an error */
      if (err) {
        console.error(chalk.red(err));
      }
      /* yay -  file was saved */
      options.repo_dir = path;
      return options;
    });
  } else {
    options.repo_dir = path;
    return options;
  }
}

/** TODO - This is a shite hack...suss out how to CD into the parent of __dirname and write a .env file... */
async function getConfig(options) {
  try {
    const response1 = await fs.readJson(__dirname + '/config.json');

    return response1;
  } catch (err) {
    const response2 = await askUserForRepoPath(options, __dirname);
    return response2;
  }
}

export async function cli(args) {
  /** Grab options passed in from terminal prompt */
  let options = parseCLIArguments(args);

  /** We need to know where to create the new repo */
  let config = await getConfig(options);

  options = await promptForProjectOptions(options);
  options.repo_path = await getConfig(options);

  console.log(chalk.blue(JSON.stringify(options)));
}
